import React, { useEffect } from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';
import { MangaLoader, impls } from './manga-loader/manga-loader';
import { unsafeWindow } from '$';


console.log(window, document, unsafeWindow)
impls.some(impl => {
  if (impl.match) {
    // 各実装に対して、その実装が対応していないURLだったらその実装は使用しない　(return falseして別の実装を探す)
    if (typeof impl.match === "string") { 
      if (!(new RegExp(impl.match, "i")).test(window.location.href)) return false
    } else if (impl.match instanceof RegExp) {
      if (!impl.match.test(window.location.href)) return false
    } else if (typeof impl.match === "function") {
      if (!impl.match(window.location.href, impl)) return false
    }

  } else { 
    // 実装がどのサイトに対するものか定義していない場合無条件でその実装は利用しない
    return false
   }
   
   // 対応した実装が見つかったら以下を実行

  ReactDOM.createRoot(
    (() => {
      const app = document.createElement('div');
      app.id = "manga-loader-app-container"
      app.style.display = "block"
      document.body.append(app);
      return app;
    })(),
  ).render(
    <React.StrictMode>
      <App impl={impl}/>
    </React.StrictMode>,
  );
})


