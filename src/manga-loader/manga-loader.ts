import { unsafeWindow } from "$";
import { CompatibleLayer } from "./compatible";

export interface GlobalVars {
    window: Window
    document: Document
}

export const GlobalVarsKeys: (keyof GlobalVars)[] = ["document", "window"]

export interface WaitFuncCompatible {
    (this: MangaLoaderImpl): boolean;
    call(thisArg: MangaLoaderImpl): boolean;
}
export type WaitFunc = (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars) => Promise<any>;
export type WaitTypes = number | string | WaitFunc

/**
 * to get src of image
 **/
export type URLSourceFunc = (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars) => Promise<string | URL>
export interface URLSourceFuncCompatible {
    (this: MangaLoaderImpl, searchIn: Element): string | URL;
    call(thisArg: MangaLoaderImpl, searchIn: Element): string | URL;
}
export type URLSource = string | URLSourceFunc

export interface NumberSourceFuncCompatible {
    (this: MangaLoaderImpl, searchIn: Element): number;
    call(thisArg: MangaLoaderImpl, searchIn: Element): number;
}
export type NumberSourceFunc = (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars) => Promise<number | undefined>
export type NumberSource = string | NumberSourceFunc

export type PagesFunc = (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars, next_url: string, page: number, ) => Promise<string | URL | undefined>
export interface PagesFuncCompatible {
    (this: MangaLoaderImpl, next_url: string, page: number, resolve: ((url: string | URL, next: any) => void), extract_function: any): void;
    call(thisArg: MangaLoaderImpl, next_url: string, page: number, resolve: ((url: string | URL, next: any) => void), extract_function: any): void;
}

export interface MangaLoaderImpl {
    /**
     * name of implements
     */
    name: string

    /**
     * when use the implements
     */
    match?: string | RegExp | ((url: string, impl: MangaLoaderImpl) => boolean);

    /**
     * string: Use the value as a CSS selector and wait until the element is loaded
     * number: wait until value[ms]
     * function: wait uiltil return value is true
     */
    wait?: WaitTypes | WaitFuncCompatible;

    /**
     * A function that returns the image URL for a page.
     * @param url 
     * @param page a page number
     * @param globalVars global variables to replace default
     */
    pages?: PagesFunc | PagesFuncCompatible

    img?: URLSource |  URLSourceFuncCompatible

    next?: URLSource |  URLSourceFuncCompatible
    numpages?: NumberSource | NumberSourceFuncCompatible 
    curpage?: NumberSource | NumberSourceFuncCompatible 

    numchaps?: NumberSource | NumberSourceFuncCompatible 
    curchap?: NumberSource | NumberSourceFuncCompatible 

    nextchap?: URLSource |  URLSourceFuncCompatible
    prevchap?: URLSource |  URLSourceFuncCompatible
}

export interface NativeMangaLoaderImpl extends MangaLoaderImpl {
    wait?: WaitTypes;
    pages?: PagesFunc
    img?: URLSource
    next?: URLSource
    numpages?: NumberSource
    curpage?: NumberSource
    numchaps?: NumberSource
    curchap?: NumberSource
    nextchap?: URLSource
    prevchap?: URLSource
}

export interface MangaLoaderOptions {
    requestInterval?: number
    waitInterval?: number
}


export class MangaLoader {
    impl: NativeMangaLoaderImpl
    iframe?: HTMLIFrameElement
    numPages?: number
    curPage?: number
    document: Document = (unsafeWindow || window).document
    private images: string[] = []
    private onAddImagesListeners: ((img_url: string) => void)[] = []
    requestInterval: number
    waitInterval: number
    loaded: boolean = false


    constructor(impl: MangaLoaderImpl, options?: MangaLoaderOptions) {
        const {requestInterval, waitInterval} = options || {}
        this.impl = new CompatibleLayer(this, impl)
        this.requestInterval = requestInterval || 500
        this.waitInterval = waitInterval || 500
        let iframe = document.getElementById("ml-if")
        // if (!(iframe instanceof HTMLIFrameElement)) {
        //     this.iframe = document.createElement("iframe")
        //     this.iframe.style.display = "none"
        //     document.body.appendChild(this.iframe)
        // } else {
        //     this.iframe = iframe
        // }
    }

    addImageAddEventListener(listener: ((img_url: string) => void)) {
        this.images.forEach(imgUrl => {
            try {
                listener(imgUrl)
            } catch {}
        });
        this.onAddImagesListeners.push(listener)
    }

    removeImageAddEventListener(listener: ((img_url: string) => void)) {
        this.onAddImagesListeners = this.onAddImagesListeners.filter(item => item != listener)
    }

    async getNumber(numberSource: NumberSource | undefined, globalVars: GlobalVars, type?: string) {
        if (typeof numberSource === "string") {
            const elem = document.querySelector(numberSource)
            if (!elem) {
                console.log("elem nul")
                return
            };
            if (type === "index") {
                return this.getIndexFromElem(elem);
            }
            
            if (elem instanceof HTMLSelectElement) {
                return elem.options.length
            }
        } else if (typeof numberSource === "function") {
            return await numberSource(this.impl, this, globalVars)
        }
    }

    getIndexFromElem(elem: Element, val?: number, invIdx?: boolean): number | undefined | null {
        if (!val) val = 0
        if (!invIdx) invIdx = false

        if (elem instanceof HTMLSelectElement) {
            var idx = elem.options.selectedIndex + 1 + val;
            if(invIdx) idx = elem.options.length - idx + 1;
            return idx;
        }
        if (elem.textContent)  { 
            const index =  parseInt(elem.textContent)
            if (!Number.isNaN(index)) return index
        }
    }

    getValueFromElem(elem: Element, val?: number): string | undefined | null {
        if (!val) val = 0

        if (elem instanceof HTMLSelectElement) {
            const option = elem.options[elem.options.selectedIndex + val] || {};
            return option.value
        }
        return elem.textContent
    }

    getTextFromElem(elem: Element, val?: number): string | undefined | null {
        if (!val) val = 0

        if (elem instanceof HTMLSelectElement) {
            const option = elem.options[elem.options.selectedIndex + val] || {};
            return option.textContent
        }
        return elem.textContent
    }

    getUrlFromElem(elem: Element, altProp?: string): string | URL | undefined | null {
        if (elem instanceof HTMLAnchorElement) {
            return elem.href || elem.getAttribute("href")
        } else if (elem instanceof HTMLImageElement) {
            if (altProp) return elem.getAttribute(altProp);
            return elem.src || elem.getAttribute("src")
        }
        console.log("elem", elem)
    }

    async getUrl(globalVars: GlobalVars, source: URLSource | undefined, next_url: string) {
        const { window, document } = globalVars;
        if (!this.curPage || !this.numPages) {
            return
        }
        console.log("pagenum", this.curPage, "pages", this.numPages)
        if (this.impl.pages) {
            return await this.impl.pages(this.impl, this, globalVars, next_url, this.curPage)
        }
        console.log("not pages")

        if (typeof source === "function") {
            return ""
        }

        if (typeof source === "string") {
            const elem = document.querySelector(source)
            if (!elem) {
                console.log("elem nul")
                return
            };
            return this.getUrlFromElem(elem)
        }
        console.log(this.impl.img)
    }

    sleep(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async waitForReady(globalVars: GlobalVars) {
        const wait = this.impl.wait
        if (typeof wait === "number") {
            await this.sleep(wait)
            return
        }
        if (typeof wait === "string") {
            while (!globalVars.document.querySelector(wait)) {
                await this.sleep(this.waitInterval)
                console.log("waiting", globalVars, globalVars.document.querySelector(wait), wait)
            }
            return
        }
        if (typeof wait === "function") {
            while (!await wait(this.impl, this, globalVars)) {
                await this.sleep(this.waitInterval)
            }
            return
        }
    }

    async load(globalVars: GlobalVars) {
        this.loaded = true
        await this.waitForReady(globalVars)
        console.log("wait ok")

        if (!this.curPage || !this.numPages) {
            this.numPages = await this.getNumber(this.impl.numpages, globalVars) || 0
            this.curPage = await this.getNumber(this.impl.curpage, globalVars, "index") || 1
            if (!this.curPage || !this.numPages) { 
                console.log("page count failed")
                return
             }
        }

        const nowURL = await this.getUrl(globalVars, this.impl.img, globalVars.window.location.href)
        if (!nowURL) {
            console.log("nowURL error")
            return
        }
        console.log("now", nowURL)
        const newURL = (nowURL instanceof URL)?nowURL.href:nowURL
        this.images.push(newURL)
        this.onAddImagesListeners.forEach(lisener => {
            try {
                lisener(newURL)
            } catch {}
        });

        this.curPage += 1
        if (this.numPages < this.curPage  ) {
            console.log("page > numPages")
            return
        }

        const nextURL = await this.getUrl(globalVars, this.impl.next, globalVars.window.location.href)
        console.log("next", nextURL)

        if (!nextURL) {
            console.log("endpage")
            return
        }

        await this.sleep(this.requestInterval)

        if (this.impl.pages) {
            await this.load(globalVars)
            return
        }

        const p = new DOMParser()
        const r = fetch(nextURL)
        const t = await (await r).text()
        const dom = p.parseFromString(t, "text/html")

        // this.iframe.src = nextURL
        // const newWindow = this.iframe.contentWindow
        // if (!newWindow) {
        //     console.log("contentWindow not found")
        //     return
        // }

        // console.log("try load content", Object.assign(newWindow))
        // await this.sleep(500)
        // // await new Promise(resolve => {newWindow.addEventListener("DOMContentLoaded", () => resolve(undefined))})
        // console.log("content loaded")

        await this.load({ ...globalVars, document: dom })


    }
}

const loadImpls =  (async () => {
    const impls: MangaLoaderImpl[] = []
    for (const importFunction of Array.from(Object.values(await import.meta.glob("./implements/*")))) {
        const mod = await importFunction() as any
        if (mod.default instanceof Array) {
            impls.concat(mod.default as MangaLoaderImpl)
        } else if (Object.getPrototypeOf(mod.default) === Object.prototype) {
            impls.push(mod.default as MangaLoaderImpl)
        }
    }
    return impls
})

export const impls: MangaLoaderImpl[] = await loadImpls()
