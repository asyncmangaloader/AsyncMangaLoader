import { GlobalVars, GlobalVarsKeys, MangaLoader, MangaLoaderImpl, NativeMangaLoaderImpl, NumberSource, NumberSourceFunc, NumberSourceFuncCompatible, PagesFunc, PagesFuncCompatible, URLSource, WaitFunc, WaitFuncCompatible, WaitTypes } from "./manga-loader";



export class CompatibleLayer implements NativeMangaLoaderImpl {
    private oridinalImpl: MangaLoaderImpl
    private loader: MangaLoader
    name: string;
    match?: string | RegExp | ((url: string, impl: MangaLoaderImpl) => boolean) | undefined;
    wait?: WaitTypes;
    pages?: PagesFunc;
    img?: URLSource;
    next?: URLSource;
    numpages?: NumberSource;
    curpage?: NumberSource;
    numchaps?: NumberSource;
    curchap?: NumberSource;
    nextchap?: URLSource;
    prevchap?: URLSource;
    

    constructor(loader: MangaLoader, impl: MangaLoaderImpl) {
        this.loader = loader
        this.oridinalImpl = impl
        this.name = impl.name
        this.match
        if (typeof this.oridinalImpl.wait === "string" || typeof this.oridinalImpl.wait === "number") {
            this.wait = this.oridinalImpl.wait
        } else if (typeof this.oridinalImpl.wait === "function") {
            if (this.isAsyncFunction(this.oridinalImpl.wait)) {
                this.wait = this.oridinalImpl.wait
            } else {
                this.wait = this.getCompatibleWaitFunc(this.oridinalImpl.wait as WaitFuncCompatible) as WaitFunc
            }
        }
        if (this.oridinalImpl.pages) {
            if (!this.isAsyncFunction(this.oridinalImpl.pages)) {
                this.pages = this.getCompatiblePagesFunc(this.oridinalImpl.pages as PagesFuncCompatible) as PagesFunc
            } else {
                this.pages = this.oridinalImpl.pages
            }
        }
        if (this.oridinalImpl.numpages) {
            if (typeof this.oridinalImpl.numpages === "string") {
                this.numpages = this.oridinalImpl.numpages
            } else if (this.isAsyncFunction(this.oridinalImpl.numpages)) {
                this.numpages = this.oridinalImpl.numpages
            } else if (typeof this.oridinalImpl.numpages === "function") {
                this.numpages = this.getCompatibleNumberFunc(this.oridinalImpl.numpages as NumberSourceFuncCompatible, "numpages") as NumberSourceFunc
            }
        }
        if (this.oridinalImpl.curpage) {
            if (typeof this.oridinalImpl.curpage === "string") {
                this.curpage = this.oridinalImpl.curpage
            } else if (this.isAsyncFunction(this.oridinalImpl.curpage)) {
                this.curpage = this.oridinalImpl.curpage
            } else if (typeof this.oridinalImpl.curpage === "function") {
                this.curpage = this.getCompatibleNumberFunc(this.oridinalImpl.curpage as NumberSourceFuncCompatible, "curpage") as NumberSourceFunc
            }
        }

        if (typeof this.oridinalImpl.img === "string") {
            this.img = this.oridinalImpl.img
        }

        if (typeof this.oridinalImpl.next === "string") {
            this.next = this.oridinalImpl.next
        }
    }

    extractInfoCompatible(selector: string | ((searchIn: Element) => string), mod: {type?: string, altProp?: string, val?: number, invIdx?: boolean}, searchIn: Element): number | string | URL | undefined | null {
        if (typeof selector === "function") {
            return selector.call(this, searchIn)
        }
        const elem = searchIn.querySelector(selector)
        if (!elem) return;
        if (!mod.type) {
        } else if (mod.type === "index") {
            return this.loader.getIndexFromElem(elem, mod.val, mod.invIdx)
        } else if (mod.type === "value") {
            return this.loader.getValueFromElem(elem, mod.val)
        } else if (mod.type === "text") {
            return this.loader.getTextFromElem(elem, mod.val)
        }

        if (elem instanceof HTMLAnchorElement || elem instanceof HTMLImageElement) {
            return this.loader.getUrlFromElem(elem, mod.altProp)
        } else if (elem instanceof HTMLSelectElement) {
            return elem.options.length
        } else if (elem instanceof HTMLUListElement) {
            return elem.children.length
        }
        return elem.textContent
    }

    getCompatiblePagesFunc(func: PagesFuncCompatible): PagesFunc {
        return async (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars, next_url: string, page: number) => {
            return await new Promise(resolve => this.getCompatible(func)(this.oridinalImpl, loader, globalVars, next_url, page, resolve, this.extractInfoCompatible))
        }
    }

    getCompatibleNumberFunc(func: NumberSourceFuncCompatible, debugname?: string): PagesFunc {
        return async (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars) => {
            console.log("numfunc", debugname)
            const ret = this.getCompatible(func)(impl, loader, globalVars, globalVars.document)
            console.log("num ret:", ret)
            return ret
        }
    }

    getCompatibleWaitFunc(func: WaitFuncCompatible, debugname?: string): WaitFunc {
        return async (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars) => {
            await new Promise(resolve => { setInterval(() => {if (func.call(this.oridinalImpl)) resolve(true)}, loader.waitInterval) })
            return true
        }
    }

    isAsyncFunction(func: Function): func is (...args: any[]) => Promise<any> {
        if (typeof func !== "function") return false
        try { if (func() instanceof Promise) return true } catch {  }
        return false
    }

    getCompatible(func: Function) {
        if (func.toString().startsWith("function")) return this.getCompatibleFunction(func);
        return this.getCompatibleArrowFunction(func)
    }

    getCompatibleArrowFunction(func: Function) {
        return (...args: any[]) => undefined
    }

    getCompatibleFunction(func: Function, debugname: string="defaultname"): (impl: MangaLoaderImpl, loader: MangaLoader, globalVars: GlobalVars, ...args: any[]) => any {
        
        const argsStart = func.toString().indexOf("(") + 1
        const argsEnd = func.toString().indexOf(")")
        let argsDefs = func.toString().slice(argsStart, argsEnd)
        
        const addArgs = "___manga_loader_impl, ___manga_loader, {" + Array.from(GlobalVarsKeys).join(",") + "}"
        argsDefs = addArgs + "," + argsDefs

        console.log(argsDefs)
        console.log(func.toString())
        const globalVarWrapStr = "const W = window; const D = document"
        let funcname =  func.toString().slice(0, argsStart-1).replace("function", "").trim()
        console.log(funcname)
        funcname = funcname.length?funcname:debugname
        const newFuncStr = `return function ${funcname}(`   + argsDefs + func.toString().slice(argsEnd, func.toString().indexOf("{", argsEnd) + 1) + globalVarWrapStr + func.toString().slice(func.toString().indexOf("{", argsEnd) + 1)
        console.log(newFuncStr)
        const newFunc = Function(newFuncStr)()
        console.log(newFunc)
        newFunc.bind(this.oridinalImpl)

        console.log(newFunc.toString())

        return newFunc
    }
    
}
