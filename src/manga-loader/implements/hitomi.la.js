export default {
    name: 'hitomi',
    match: "^http(s)?://hitomi.la/reader/[0-9]+.html",
    img: '#comicImages img',
    next: '#comicImages img',
    numpages: function() {
      return W.our_galleryinfo.length;
    },
    curpage: function() {
      return parseInt(W.curPanel);
    },
    pages: function(url, num, cb, ex) {
      const format = W.our_galleryinfo[num-1].hasavif?'avif':"webp"
      cb(W.url_from_url_from_hash(W.galleryinfo.id, W.our_galleryinfo[num-1], format, format, 'a'), num);
    },
    wait: '#comicImages img'
  }