import { useEffect, useRef, useState } from 'react';
import styles from "./App.module.css"
import { MangaLoader, MangaLoaderImpl, MangaLoaderOptions } from './manga-loader/manga-loader';
import { GM_getValue, GM_setValue, unsafeWindow } from '$';

interface MangaLoaderConfig extends MangaLoaderOptions {
  autoLoad?: boolean
  pageStyle?: "single" | "double" | ""
  direction?: "left-start" | "right-start" | ""
}

interface ConfigProps {
  currentConfig: MangaLoaderConfig
  onChangeCurrentConfig: ((e: MangaLoaderConfig) => void)
  onClose: () => void
  show: boolean,
  impl: MangaLoaderImpl
}

function Config({ currentConfig, onChangeCurrentConfig, show, impl, onClose }: ConfigProps) {
  const [nowConfig, setNowConfig] = useState<MangaLoaderConfig>(currentConfig)
  const [target, setTarget] = useState<"current" | "site" | "global">("current")
  const autoLoad = typeof nowConfig.autoLoad === "undefined" ? "" : (nowConfig.autoLoad ? "true" : "false")

  function onChangeConfig(newConfig: MangaLoaderConfig) {
    setNowConfig(newConfig)
  }
  function onChangeTarget(v: string) {
    if (v === "current") {
      setTarget(v)
      setNowConfig(currentConfig)
    } else if (v === "global") {
      setTarget(v)
      setNowConfig(JSON.parse(GM_getValue("default", "{}")) || {})
    } else if (v === "site") {
      setTarget(v)
      setNowConfig(JSON.parse(GM_getValue(impl.name, "{}")) || {})
    }
  }

  function onSaveConfig() {
    if (target === "current") {
      onChangeCurrentConfig(nowConfig)
    } else if (target === "global") {
      GM_setValue("default", JSON.stringify(nowConfig))
    } else if (target === "site") {
      GM_setValue(impl.name, JSON.stringify(nowConfig))
    }
    onClose()
  }

  function onReset() {
    onChangeTarget(target)
  }

  if (!show) return;

  return (
    <>
      <div className={styles.form}>
        <div className={styles.form_row}>
          <label>config target </label>
          <select value={target} onChange={(e) => onChangeTarget(e.target.value)}>
            <option value="current">current</option>
            <option value="site">site</option>
            <option value="global">global</option>
          </select>
        </div>
        {target !== "current" &&
          <div className={styles.form_row}>
            <label>autoload</label>
            <select value={autoLoad} onChange={e => onChangeConfig({ ...nowConfig, autoLoad: e.target.value ? Boolean(e.target.value) : undefined })}>
              <option value={""}>Extend</option>
              <option value={"true"}>Enabled</option>
              <option value={"false"}>Disabled</option>
            </select>
          </div>}
        <div className={styles.form_row}>
          <label>pgae style </label>
          <select value={nowConfig.pageStyle || ""} onChange={(e) => { if (["single", "double", ""].includes(e.target.value)) { onChangeConfig({ ...nowConfig, pageStyle: e.target.value ? e.target.value as any : undefined }); console.log(e.target.value) } }}>
            {target !== "current" && <option value={""}>extends</option>}
            <option value={"single"}>single</option>
            <option value={"double"}>double</option>
          </select>
        </div>
        <div className={styles.form_row}>
          <label>direction </label>
          <select value={nowConfig.direction || ""} onChange={(e) => { if (["left-start", "right-start", ""].includes(e.target.value)) { onChangeConfig({ ...nowConfig, direction: e.target.value ? e.target.value as any : undefined }); console.log(e.target.value) } }}>
            {target !== "current" && <option value={""}>extends</option>}
            <option value={"left-start"}>left-start</option>
            <option value={"right-start"}>right-start</option>
          </select>
        </div>
        <div style={{gridColumn: "span 2"}}>
          <button type='button' onClick={onSaveConfig}>Save</button>
          <button type='button' onClick={onReset}>Reset</button>
          <button type='button' onClick={()=>onClose()} style={{float: "right"}}>Close</button>
        </div>
      </div>
    </>
  )
}


interface AppProps {
  impl: MangaLoaderImpl
}

function App({ impl }: AppProps) {
  const defaultConfig: MangaLoaderConfig = { autoLoad: false, pageStyle: "single", direction: "left-start", ...JSON.parse(GM_getValue("default", "{}")) || {} }
  const initialConfig: MangaLoaderConfig = { ...defaultConfig, ...(JSON.parse(GM_getValue(impl.name, "{}")) || {}) }

  const loader = useRef(new MangaLoader(impl, initialConfig))
  const [config, setConfig] = useState<MangaLoaderConfig>(initialConfig)
  const [show, setShow] = useState(false)
  const [inited, setInited] = useState(false)
  const [showConfigPanel, setShowConfigPanel] = useState(false)
  const [images, setImages] = useState<Array<string>>([])
  const queue = useRef<Array<string>>([])

  useEffect(() => {
    function onImageAdd(url: string) {
      setImages((images) => images.concat([url]))
      if (initialConfig.autoLoad && !inited) {
        setShow(true)
        setInited(true)
      }
    }
    loader.current.addImageAddEventListener(onImageAdd)
    return () => {
      loader.current.removeImageAddEventListener(onImageAdd)
    }
  }, [inited])

  useEffect(() => {
    function hideChildren() {
      for (const elem of document.body.children) {
        if (elem.id !== "manga-loader-app-container") {
          console.log("hide", elem, styles.hidden)
          elem.classList.add(styles.hidden);
        }
      }
    }

    function showChildren() {
      for (const elem of document.body.children) {
        if (elem.classList.contains(styles.hidden)) {
          console.log("show", elem, styles.hidden)
          elem.classList.remove(styles.hidden);
        }
      }
    }
    if (show) {
      hideChildren();
      if (!loader.current.loaded) {
        loader.current.load({ document, window: unsafeWindow as Window });
      }
    }
    return () => showChildren()
  }, [show])
  console.log(initialConfig.autoLoad , loader , !loader.current.loaded)
  if (initialConfig.autoLoad && loader && !loader.current.loaded) {
    console.log("load start")
    loader.current.load({ document, window: unsafeWindow as Window });
  }


  return (
    <>
      <div className={styles.center_box}>
        <div className={[styles.images_box, show ? "" : styles.hidden, config.pageStyle === "single" ? "" : styles.double, config.direction ? styles[config.direction] : ""].join(" ")}>
          {images.map((src, index) => <div className={styles.image} key={index}><img onError={(e) => { if ("src" in e.target) e.target.src = e.target.src }} src={src} loading='lazy'></img></div>)}
        </div>
      </div>
      <div className={styles.control_box}>
        <div className={styles.panel} style={{ display: (showConfigPanel && show) ? "grid" : "none", backgroundColor: "white", padding: "20px" }}>
          <button type="button" onClick={() => setShow((now) => !now)}>{show ? "Unload Manga" : "Load Manga"}</button>
          <Config onClose={() => setShowConfigPanel(false)} impl={impl} currentConfig={config} onChangeCurrentConfig={setConfig} show={show && showConfigPanel} />
        </div>
        <div className={styles.mini_panel}>
          {show ? <a onClick={() => setShowConfigPanel(now => !now)}>⚙️</a> : <button type="button" onClick={() => setShow((now) => !now)}>Load Manga</button>}
        </div>

      </div>
    </>
  );
}

export default App;
